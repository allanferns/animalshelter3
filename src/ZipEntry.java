public class ZipEntry {
    private int zipCode;
    private String cityName;

    public ZipEntry(int zipCode, String cityName){
        this.zipCode = zipCode;
        this.cityName = cityName;
    }
    public void printZipEntry(){
        System.out.println("The zipcode is " + this.zipCode);
        System.out.println("The city name is " + this.cityName);
    }
}
