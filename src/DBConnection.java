
import jdk.nashorn.internal.codegen.CompilerConstants;
import org.omg.CORBA.PUBLIC_MEMBER;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBConnection
{
    Connection con;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    List<ZipEntry> zipEntries;
    ArrayList<Integer> vacantCages;
    private ArrayList<ReceiptDetails> receiptDetailsArrayList;

    public DBConnection(){

        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=ANIMAL_SHELTER2","sa","123456");
            System.out.println("DATABASE READY");
        }catch (Exception e){
            e.getMessage();
        }
    }

    public void closePSConnection(){
        try{
            con.close();
            ps.close();
            cs.close();
            System.out.println("CONNECTION CLOSED");
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void getZipEntry(){
        int zipCode;
        String cityName;
        ZipEntry tempEntry;
        try {
            ps = con.prepareStatement("SELECT * FROM Zip");
            rs = ps.executeQuery();
            while(rs.next()){
                zipCode = rs.getInt(1);
                cityName = rs.getString(2);
                tempEntry = new ZipEntry(zipCode, cityName);
                tempEntry.printZipEntry();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void testStoredProcedure(){
        try{
            cs = con.prepareCall("{CALL TEST}");
            cs.execute();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public ArrayList<Integer> getVacantCages(int weekNumber, int yearNumber){
        vacantCages = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL getVacantCages (?, ?)}");
            cs.setInt(1, weekNumber);
            cs.setInt(2, yearNumber);
            rs = cs.executeQuery();
            while(rs.next()){
                vacantCages.add(rs.getInt(1));
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return vacantCages;
    }

    public String getCustomerDetails(String tlfNo){
        String customerName = "";
        try{
            cs = con.prepareCall("{CALL getCustomerName (?)}");
            cs.setString(1, tlfNo);
            rs = cs.executeQuery();
            while (rs.next()){
                customerName = rs.getString(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return customerName;
    }

    public void addToZipTable(int zipCode, String cityName){
        try{
            cs = con.prepareCall("{CALL addToZipTable (?, ?)}");
            cs.setInt(1, zipCode);
            cs.setString(2, cityName);
            cs.execute();
        }catch (Exception e){
            System.out.println("Already in the system");
        }
    }

    public boolean addToCustomerTable(String tlfNo, String name, String address, int zip){
        try{
            cs = con.prepareCall("{CALL addToCustomerTable (?,?,?,?)}");
            cs.setString(1, tlfNo);
            cs.setString(2, name);
            cs.setString(3, address);
            cs.setInt(4, zip);
            cs.execute();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean checkIfPetIsRegistered(String petName, String tlfNo){
        int count = 0;
        try{

            ps = con.prepareStatement("select COUNT(PetID) from Pet where PetName = ? and cust_tlf = ?");
            ps.setString(1, petName);
            ps.setString(2, tlfNo);
            rs = ps.executeQuery();
            while (rs.next()){
                count = rs.getInt(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if(count > 0){
            return true;
        }else{
            return false;
        }
    }

    public void addPetToTable(String petName, String breed, String custTlf){
        try{
            cs = con.prepareCall("{CALL addToPetTable (?,?,?)}");
            cs.setString(1, petName);
            cs.setString(2, breed);
            cs.setString(3, custTlf);
            cs.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void createReceipt(){
        try{
            cs = con.prepareCall("{CALL createReceipt}");
            cs.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void createBooking(String petName, String tlf, int cageID, int weekno, int yearno, int receiptID){
        try{
            cs = con.prepareCall("{CALL createBooking (?,?,?,?,?,?)}");
            cs.setString(1, petName);
            cs.setString(2, tlf);
            cs.setInt(3, cageID);
            cs.setInt(4, weekno);
            cs.setInt(5, yearno);
            cs.setInt(6, receiptID);
            cs.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int getReceiptID(){
        int receiptID = 0;
        try{
            cs = con.prepareCall("{CALL getReceiptID}");
            rs = cs.executeQuery();
            while (rs.next()){
                receiptID = rs.getInt(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return receiptID;
    }

    public void calculateReceipt(int receiptID, int feePerBooking){
        try{

            cs = con.prepareCall("{CALL calculateAndInsertFee(?,?)}");
            cs.setInt(1, receiptID);
            cs.setInt(2, feePerBooking);
            cs.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public ArrayList<ReceiptDetails> getDetailsForReceipt(int receiptID){
        receiptDetailsArrayList = new ArrayList<>();
        try{
            cs = con.prepareCall("{CALL getDetailsForReceipt (?)}");
            cs.setInt(1, receiptID);
            rs = cs.executeQuery();
            while(rs.next()){
                String phoneNumber = rs.getString(1);
                String name = rs.getString(2);
                String petName = rs.getString(3);
                int rID = rs.getInt(4);
                int weekNum = rs.getInt(5);
                int yearNum = rs.getInt(6);
                receiptDetailsArrayList.add(new ReceiptDetails(phoneNumber, name, petName, rID, weekNum, yearNum));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return receiptDetailsArrayList;
    }

    public int getFeeFromDB(int receiptID){
        int fee = 0;
        try{
            ps = con.prepareStatement("SELECT fee FROM Receipt WHERE ReceiptID = ?");
            ps.setInt(1, receiptID);
            rs = ps.executeQuery();
            while(rs.next()) {
                fee = rs.getInt(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return fee;
    }

    public void cleanUpTables(){
        try{
            cs = con.prepareCall("{CALL cleanUPTables}");
            cs.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
