
USE ANIMAL_SHELTER2;
GO

--tlf, name, address, zip
CREATE PROCEDURE addToCustomerTable @tlfNo VARCHAR(8), @name VARCHAR(20), @address VARCHAR(50), @zip INTEGER
	AS
		BEGIN
		INSERT INTO Customer(phoneNumber, CustomerName, Address, ZipCode)
		VALUES(@tlfNo, @name, @address, @zip)
		END