import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


import javax.xml.soap.Text;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;


public class Main extends Application {
    TextField weekNumber;
    Button searchVacancies;
    ArrayList<Integer> vacantCages;
    Stage mainStage;
    Button goToRegistrationCheck;
    Group root2;
    Scene scene2;
    DBConnection dbConnection = new DBConnection();
    ArrayList<CheckBox> checkBoxArrayList;
    String telephoneNo = "";
    int numOfVacantCages;

    //This holds the receiptID for all the bookings.
    // Placed outside because we want it changed just once and the checks will be inside the event listener
    int receiptID = 0;



    @Override
    public void start(Stage stage) throws Exception {
        //Set up the database connection
        //Clean up the tables
        dbConnection.cleanUpTables();

        //dbConnection.getZipEntry();


        //Set up the initial window
        Group root = new Group();
        Scene scene = new Scene(root, 300, 200);
        mainStage = new Stage();
        mainStage.setScene(scene);
        mainStage.setTitle("Animal Shelter");
        mainStage.show();

        //Set up the registration check window


        //Constructors here
        weekNumber = new TextField();
        TextField yearNumber = new TextField();
        searchVacancies = new Button("Search Vacancies");
        goToRegistrationCheck = new Button("Go to registration check");
        vacantCages = new ArrayList<>();


        //Add to the initial root here
        root.getChildren().addAll(weekNumber, searchVacancies, goToRegistrationCheck, yearNumber);
        weekNumber.setLayoutX(65);
        weekNumber.setLayoutY(10);
        weekNumber.setPrefWidth(45);
        weekNumber.setPromptText("Week");
        yearNumber.setLayoutX(10);
        yearNumber.setLayoutY(10);
        yearNumber.setPrefWidth(45);
        yearNumber.setPromptText("Year");
        searchVacancies.setLayoutX(120);
        searchVacancies.setLayoutY(10);

        goToRegistrationCheck.setLayoutX(10);
        goToRegistrationCheck.setLayoutY(100);

        Label dispVacancies = new Label("Number of vacancies: " + "N/A");
        dispVacancies.setLayoutX(10);
        dispVacancies.setLayoutY(50);
        root.getChildren().add(dispVacancies);



        searchVacancies.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                int weekNumberOfSearch = Integer.parseInt(weekNumber.getText());
                int yearNumberOfSearch = Integer.parseInt(yearNumber.getText());
                ArrayList<Integer> temp = dbConnection.getVacantCages(weekNumberOfSearch, yearNumberOfSearch);
                numOfVacantCages = temp.size();
                dispVacancies.setText("Number of vacancies: " + numOfVacantCages);

            }
        });

        goToRegistrationCheck.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                changeToRegistrationCheckScene();
            }
        });

        mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                dbConnection.closePSConnection();
                Platform.exit();
                System.exit(0);
            }
        });
    }

    public void changeToRegistrationCheckScene(){
        //This grabs the style sheet
        //Application.setUserAgentStylesheet(getClass().getResource("/Style.css").toExternalForm());
        //Initialise the new scene stuff here
        root2 = new Group();
        scene2 = new Scene(root2, 300, 200);
        mainStage.setTitle("Registration stage");
        mainStage.setScene(scene2);
        Label registrationStatus = new Label();
        Button goToRegistrationScene = new Button("Proceed to registration");
        Button goToBookingScene = new Button("Proceed to booking");

        //Set up the text field to enter telephone number
        TextField custRegCheck = new TextField();
        custRegCheck.setPromptText("Enter tlf no.");
        custRegCheck.setLayoutX(10);
        custRegCheck.setLayoutY(10);
        custRegCheck.setPrefWidth(100);

        //Set up the button to check registration
        Button checkRegBtn = new Button("Check Registration");
        checkRegBtn.setLayoutX(130);
        checkRegBtn.setLayoutY(10);

        //Place the registrationstatus label
        registrationStatus.setLayoutX(10);
        registrationStatus.setLayoutY(80);

        //Place the goToregScene button
        goToRegistrationScene.setLayoutX(10);
        goToRegistrationScene.setLayoutY(130);

        //Place the gotobooking button
        goToBookingScene.setLayoutX(10);
        goToBookingScene.setLayoutY(130);

        root2.getChildren().addAll(custRegCheck, checkRegBtn, registrationStatus);

        checkRegBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                String tlfNo = custRegCheck.getText();
                telephoneNo = tlfNo;
                String name = dbConnection.getCustomerDetails(tlfNo);
                if(name.equals("")){
                    registrationStatus.setText("Not in the system");
                    try{
                        root2.getChildren().add(goToRegistrationScene);
                    }catch (Exception e){
                        System.out.println("just duplicates");
                    }
                    root2.getChildren().removeAll(goToBookingScene);
                }else{
                    registrationStatus.setText("Currently registered as " + name);
                    root2.getChildren().removeAll(goToRegistrationScene);
                    try{
                        root2.getChildren().add(goToBookingScene);
                    }catch (Exception e){
                        System.out.println("Just duplicates");
                    }

                }
            }
        });

        goToRegistrationScene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                goToRegistration();
            }
        });

        goToBookingScene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                goToBookingScene();
            }
        });


    }

    public void goToRegistration(){
        //This grabs the style sheet
        //Application.setUserAgentStylesheet(getClass().getResource("/Style.css").toExternalForm());
        //Initialise the new scene
        Group root3 = new Group();
        Scene scene3 = new Scene(root3, 200, 300);
        mainStage.setScene(scene3);
        mainStage.setTitle("Registration Page");


        //Add the textfields to enter all the data
        TextField tlf = new TextField();
        TextField customerName = new TextField();
        TextField customerAddress = new TextField();
        TextField zipCode = new TextField();
        TextField city = new TextField();

        //Place the labels
        tlf.setPromptText("Enter tlf number");
        tlf.setLayoutX(10);
        tlf.setLayoutY(10);

        customerName.setPromptText("Enter Name");
        customerName.setLayoutX(10);
        customerName.setLayoutY(50);

        customerAddress.setPromptText("Enter address");
        customerAddress.setLayoutX(10);
        customerAddress.setLayoutY(100);

        zipCode.setPromptText("Enter zip code");
        zipCode.setLayoutX(10);
        zipCode.setLayoutY(150);

        city.setPromptText("Enter City name");
        city.setLayoutX(10);
        city.setLayoutY(200);



        //Add and place a button to trigger the stored procedures
        Button registerDetails = new Button("Register");
        registerDetails.setLayoutX(10);
        registerDetails.setLayoutY(250);

        root3.getChildren().addAll(tlf, customerName, customerAddress, zipCode, city, registerDetails);

        registerDetails.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                int code = Integer.parseInt(zipCode.getText());
                String cityName = city.getText();
                dbConnection.addToZipTable(code, cityName);

                String tlfNo = tlf.getText();
                telephoneNo = tlfNo;
                String custName = customerName.getText();
                String custAddress = customerAddress.getText();

                if(dbConnection.addToCustomerTable(tlfNo, custName, custAddress, code)){
                    //Move to confirmation page
                    goToTransitionScene();
                }else{
                    //There was an error and the details were not added to the table. Check if you are already in the system
                    System.out.println("Check if you are already in the system");
                }
            }
        });
        //First add the new zip and city to the zip table
        //Then add the rest of the details to the Customer table
    }

    public void goToTransitionScene(){
        Group root4 = new Group();
        Scene scene4 = new Scene(root4, 300, 100);
        mainStage.setScene(scene4);
        mainStage.setTitle("Confirmation");

        //Set up the confirmation label
        Label confirmation = new Label("The details were successfully added to the database");
        confirmation.setLayoutX(10);
        confirmation.setLayoutY(10);

        //Set up the button to proceed to creating a booking
        Button proceedToBooking = new Button("Click to proceed");
        proceedToBooking.setLayoutX(10);
        proceedToBooking.setLayoutY(50);
        root4.getChildren().addAll(confirmation, proceedToBooking);

        proceedToBooking.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                goToBookingScene();
            }
        });
    }

    public void goToBookingScene(){
        //When we enter the booking scene, the receipt is created
        dbConnection.createReceipt();
        receiptID = dbConnection.getReceiptID();

        //This grabs the style sheet
        //Application.setUserAgentStylesheet(getClass().getResource("/Style.css").toExternalForm());
        Group root5 = new Group();
        Scene scene5 = new Scene(root5, 300, 350);
        mainStage.setScene(scene5);
        mainStage.setTitle("Booking scene");

        //Add a button and a text field at the top to check for vacancies
        TextField checkWeekNumber = new TextField();
        checkWeekNumber.setLayoutX(65);
        checkWeekNumber.setLayoutY(10);
        checkWeekNumber.setPrefWidth(45);
        checkWeekNumber.setPromptText("Week");

        //Add a text field to also check the year number
        TextField checkYearNumber = new TextField();
        checkYearNumber.setLayoutX(10);
        checkYearNumber.setLayoutY(10);
        checkYearNumber.setPrefWidth(45);
        checkYearNumber.setPromptText("Year");

        Button showVacancies = new Button("Show vacancies");
        showVacancies.setLayoutX(170);
        showVacancies.setLayoutY(10);

        root5.getChildren().addAll(checkWeekNumber, showVacancies, checkYearNumber);

        //Set up the label to show number of vacancies
        Label numOfVacancies = new Label();
        numOfVacancies.setLayoutX(10);
        numOfVacancies.setLayoutY(50);
        root5.getChildren().addAll(numOfVacancies);

        //Set up a button to take the user to the receipt scene
        Button proceedToReceipt = new Button("Proceed to receipt");
        proceedToReceipt.setLayoutX(10);
        proceedToReceipt.setLayoutY(300);
        root5.getChildren().add(proceedToReceipt);

        proceedToReceipt.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                goToReceiptScene();
            }
        });

        showVacancies.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try{
                    vacantCages = dbConnection.getVacantCages(Integer.parseInt(checkWeekNumber.getText()), Integer.parseInt(checkYearNumber.getText()));
                    numOfVacancies.setText("Number of vacancies is " + vacantCages.size());
                }catch (Exception e){
                    System.out.println("Invalid input");
                }
                System.out.println(vacantCages.toString());
            }
        });

        //in the booking scene, you register the animal and click confirm to automatically assign it a cage

        //Add stuff to this scene to add details about pets

        TextField petName = new TextField();
        TextField breedName = new TextField();
        TextField custTlf = new TextField(telephoneNo);

        //Position all the text fields
        petName.setLayoutX(10);
        petName.setLayoutY(100);
        petName.setPromptText("Enter the pet name");
        breedName.setLayoutX(10);
        breedName.setLayoutY(150);
        breedName.setPromptText("Enter the breed's name");
        custTlf.setLayoutX(10);
        custTlf.setLayoutY(200);
        custTlf.setPromptText("Enter the customer's PK/tlf");

        //Add a button to trigger a stored procedure
        Button addToPetTable = new Button("Register Pet");

        addToPetTable.setLayoutX(10);
        addToPetTable.setLayoutY(250);

        root5.getChildren().addAll(petName, breedName, custTlf, addToPetTable);



        addToPetTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(vacantCages.size() == 0){
                    numOfVacancies.setText("Max Capacity for this week");
                    return;
                }

                //Get the data
                String nameofPet = petName.getText();
                String nameOfBreed = breedName.getText();
                String tlfOfCust = custTlf.getText();

                //Number of steps here

                //add to the pet table DONE
                //Get the receipt ID
                //Get the cage ID
                //Then create the booking

                if(!dbConnection.checkIfPetIsRegistered(nameofPet, tlfOfCust)){
                    dbConnection.addPetToTable(nameofPet, nameOfBreed, tlfOfCust);
                }else{
                    System.out.println("Pet is already registered");
                }
                //Everything I need: pet name, tlfno, cageID, weeknumber, receiptID
                //what I still need:cageID, weeknumber
                int cageID = vacantCages.get(0);
                int weekNumber = Integer.parseInt(checkWeekNumber.getText());
                int yearNumber = Integer.parseInt(checkYearNumber.getText());
                vacantCages.remove(0);
                vacantCages.trimToSize(); //This is also not fixed

                dbConnection.createBooking(nameofPet, tlfOfCust, cageID, weekNumber,yearNumber, receiptID);
                //System.out.println(vacantCages.toString());

                //Clear all except the telephone field
                //Upgrade usability by auto entering the phone number from before
                petName.setText("");
                breedName.setText("");
                vacantCages = dbConnection.getVacantCages(Integer.parseInt(checkWeekNumber.getText()), Integer.parseInt(checkYearNumber.getText()));
                numOfVacancies.setText("Number of vacancies is " + vacantCages.size());
                if(vacantCages.size() == 0){
                    numOfVacancies.setText("Max Capacity for this week");
                }
            }
        });

    }

    public void goToReceiptScene(){
        //This grabs the style sheet
        //Application.setUserAgentStylesheet(getClass().getResource("/Style.css").toExternalForm());
        //Set up the scene
        Group root6 = new Group();
        Scene scene6 = new Scene(root6, 250, 300);
        mainStage.setScene(scene6);
        mainStage.setTitle("Receipt");

        //Parameter for the stored procedure
        int feePerBooking = 500;
        dbConnection.calculateReceipt(receiptID, feePerBooking);

        //Get the information from the database
        ArrayList<ReceiptDetails> receiptDetailsArrayList = dbConnection.getDetailsForReceipt(receiptID);
        String nameOfOwner = receiptDetailsArrayList.get(0).getName();
        String custTlf = receiptDetailsArrayList.get(0).getPhoneNumber();
        int weekNumber = receiptDetailsArrayList.get(0).getWeekNumber();
        int yearNumber = receiptDetailsArrayList.get(0).getYearNumber();
        int receiptID = receiptDetailsArrayList.get(0).getReceiptID();


        //Set up the labels to display information in the window
        Label ownerName = new Label("Owners name: " + nameOfOwner);
        Label ownertlf = new Label("Owner's telephone: " + custTlf);
        Label time = new Label("Week/year: " + weekNumber +"/" + yearNumber);
        Label rID = new Label("ReceiptID: " + receiptID);


        //position the labels
        ownerName.setLayoutX(10);
        ownerName.setLayoutY(10);

        ownertlf.setLayoutX(10);
        ownertlf.setLayoutY(50);

        time.setLayoutX(10);
        time.setLayoutY(100);

        rID.setLayoutX(10);
        rID.setLayoutY(150);

        root6.getChildren().addAll(ownerName, ownertlf,  rID, time);

        String petNames = "";

        for (int i = 0; i < receiptDetailsArrayList.size(); i++) {
            petNames = petNames + ", " + receiptDetailsArrayList.get(i).getPetName();
        }

        //Set up the label for the pet name
        Label listOfPets = new Label("Pets booked: " + petNames);
        listOfPets.setLayoutX(10);
        listOfPets.setLayoutY(200);

        //Set up the label to display the fee
        int feeForLabel = dbConnection.getFeeFromDB(receiptID);
        Label feeLabel = new Label("The fee is: " + feeForLabel);
        feeLabel.setLayoutX(10);
        feeLabel.setLayoutY(250);

        root6.getChildren().addAll(listOfPets, feeLabel);


    }


}
