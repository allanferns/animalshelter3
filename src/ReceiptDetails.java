public class ReceiptDetails {
    private String phoneNumber;
    private String name;
    private String petName;
    private int receiptID;
    private int weekNumber;
    private int yearNumber;

    public ReceiptDetails(String phoneNumber, String name, String petName, int receiptID, int weekNumber, int yearNumber) {
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.petName = petName;
        this.receiptID = receiptID;
        this.weekNumber = weekNumber;
        this.yearNumber = yearNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPetName() {
        return petName;
    }

    public int getYearNumber() {
        return yearNumber;
    }

    public int getReceiptID() {
        return receiptID;
    }

    public int getWeekNumber() {
        return weekNumber;
    }
}
