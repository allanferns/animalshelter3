USE ANIMAL_SHELTER2;
GO

CREATE PROCEDURE createBooking @petName varchar(20), @custTlf varchar(8), @cageID INTEGER, @weekNumber INTEGER, @receiptID INTEGER
	AS
		BEGIN
		--FIRST GET THE PETID
		DECLARE @petID INTEGER = (SELECT PetID FROM Pet WHERE PetName = @petName AND cust_tlf = @custTlf);

		--INSERT INTO THE BOOKING TABLE
		INSERT INTO Booking(PetID, CageID, WeekNumber, ReceiptID)
		VALUES(@petID, @cageID, @week